COMP6841 - Something Awesome - Homemade Keylogger

When I initially started the project, I wanted to write a keylogger for
Windows (since most people are running Windows, I wanted my project to have a
real world use case). I ended up writing a keylogger using a module called
pyHook, but I didn't feel like I learned anything. I had written some lines 
of code implementing pyHook, but I had no idea what was happening under the hood. 
So, I scrapped the keylogger and started again but this time in C, and for my 
own system, linux. 

The keylogger outputs keys captured to /var/log/KEYLOG.txt. This can be changed
to whatever location/file you want. You could store it in the /tmp folder but
some linux systems wipe this folder clean on startup. This may or may not cause
problems for you, depending on the use case. I've implemented a shell script 
called keyloghelper.sh to help manage the output file (to wipe it clean every boot
and to send the data to myself). The shell script can be edited to change the 
receiver email address to whatever you want. 

In keylog.c, int fd is reading outputs from "/dev/input/event3." In my machine, 
event3 is the keyboard. Most machines will be slightly different. To find the event 
number for your machine cat "/proc/bus/input/devices | more" and look for event# 
under keyboard. 

Also, keylog.c needs to be executed with sudo previliges, otherwise the return 
from "/dev/input/event3" will be -1. There's a few ways you could achieve this, 
one of them is to use systemctl. Essentially you place a .service file in 
/lib/systemd/system which runs on under root on startup(Google for further instructions). 

Another method is to use init.d or profile.d, where the contents get executed on startup.
A word of caution though, I messed around with my startup folders
and ended up not being able to login normally. I had to open a command line
before I got past the login screen. There was a moment where I thought I would
have to wipe everything and do a reinstall of the OS. 

Once you're collecting outpus from dev/input/event3 (whatever the number is in 
your case), you'll need to cat /usr/include/linux/input-event-codes.h to decipher
the gibberish coming back from event3. This is the what the 'map' is doing 
in keylog.c

The helper script also runs on startup - it emails a copy of KEYLOG.txt (hence 
the keys types during previous login), then deletes it. Then, keylog.c creates
a new KEYLOG.txt and the cycle continues. 

A final word on the helper script - trying to send the email right away will not
work as there is a slight delay between startup and connecting to the internet. 
You could use the 'sleep' command, or have an infite loop like I have and only 
break out once the command id done. I'm not sure why, but in both cases this
pauses the system during startup by a few seconds. Obviously, savvy tech users
will notice something is up and start investigating. 
Commands liek 'at' and 'cron' can be used to execute commands at certain times...


